public class S1 {
}
interface I{}

class B implements I{
    D d;
    private long t;
    B(){}
    public void adD(D dint){
        this.d=dint;
    }
}

class C{
    public void metB(B b){

    }
}
class D{
    F f;
    G g;
    public void met1(int i){}
    public D(F fint,G gint){this.f=fint;this.g=gint;} //explicit
    public D(){} //implicit
}

class E{
    D d;
    public E(){
        d=new D();
    }
}

class F{
    public void n (String s){}
    F(){}
}

class G{
    public double met3(){
        double x=2.37;
        return x;
    }
    G(){}
}
