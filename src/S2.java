import javax.swing.*;
import java.sql.SQLOutput;

public class S2 {
    public static void main(String[] args) {

        Th LOt1 = new Th();
        Th LOt2 = new Th();
        Th LOt3 = new Th();   //create threads
        Th LOt4 = new Th();

        LOt1.setName("LO-Thread1");
        LOt2.setName("LO-Thread2");  //Name threads
        LOt3.setName("LO-Thread3");
        LOt4.setName("LO-Thread4");

        LOt1.start();
        LOt2.start();
        LOt3.start();  //Start threads
        LOt4.start();
    }
}
class Th extends Thread{
    public Th(){} //constructor
    public void run(){
         int i;  //run method will be executed on a separate thread when called with start()
         for (i=1;i<=19;i++) {
             System.out.println(this.getName() +" - "+ i);
             try {
                 this.sleep(1000); //delay of 1 second
             } catch (InterruptedException e) {
                 e.printStackTrace();
             }
         }
    }
}


